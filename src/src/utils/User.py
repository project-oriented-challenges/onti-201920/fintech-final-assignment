from datetime import datetime
from telebot.types import Message
from web3 import Web3
import logging

from utils import mongotools
from utils import messages
from utils import tools
from utils import blockchaintools
from utils.config import AMOUNT_OF_SYMBOLS_TO_COMPARE_IDS, BOT_ADMIN
from ServerModule.Server import get_unconfirmed_transactions_from_address

from utils.bot_stat import return_db_stats_info


class User:
  """
    User instance

    Before processing a user request (command or text), 
    a User instance with all parameters is created 
    It processed, and changes are loaded to the database.
    """
  def __init__(self, alias, registered, address, in_some_group, notification):
    self.alias = alias
    self.registered = registered
    self.address = address
    self.in_some_group = in_some_group
    self.notification = notification

  def action(self, message: Message):
    text = message.text

    # If text is not a command
    if not text.startswith('/'):
      return self.__text(message)

    if text.startswith('/start'):
      return self.__start(message)

    elif text.startswith('/help'):
      return self.__help(message)

    elif text.startswith('/register'):
      return self.__register(message)

    elif text.startswith('/listgroups'):
      return self.__listchats(message)

    elif text.startswith('/listusers'):
      return self.__listusers(message)

    elif text.startswith('/reset'):
      return self.__reset(message)

    elif text.startswith('/deploy'):
      return self.__deploy(message)

    elif text.startswith('/set_contract'):
      return self.__set_contract(message)

    elif text.startswith('/get_contract'):
      return self.__get_contract(message)

    elif text.startswith('/withdraw'):
      return self.__withdraw(message)

    elif text.startswith('/available_withdraw'):
      return self.__available_withdraw(message)

    # Following commands require user to be registered
    # so check if user is registered
    elif not self.registered:
      return messages.NOT_REGISTERED

    elif text.startswith('/remove'):
      return self.__remove(message)

    elif text.startswith('/change'):
      return self.__change(message)

    elif text.startswith('/info'):
      return self.__info(message)

    elif text.startswith('/getbalance'):
      return self.__getbalance(message)

    elif text.startswith('/list'):
      return self.__list(message)

    elif text.startswith('/deltx'):
      return self.__delete_transaction(message)

    elif text.startswith('/notification'):
      return self.__notification(message)

    elif text.startswith('/premium'):
      return self.__premium(message)

    else:
      return messages.SEE_HELP

  def __text(self, message):
    if 'send' in message.text.lower():
      return self.__send(message)
    return messages.SEE_HELP

  def __start(self, message):
    if self.in_some_group:
      return messages.START_MESSAGE_WITH_GROUP
    return messages.START_MESSAGE_WITHOUT_GROUP

  def __help(self, message):
    return messages.HELP_MESSAGE

  def __register(self, message, changed=False):
    if not self.in_some_group:
      return messages.NOT_IN_CHAT

    if self.registered:
      return messages.ALREADY_REGISTERED

    try:
      assert len(message.text.split()) == 2
      address = blockchaintools.check_pub_key(message.text.split()[1])
    except (ValueError, IndexError, AssertionError):
      return messages.ADDRESS_IS_INCORRECT

    self.address = address
    self.registered = True
    if changed:
      return messages.CHANGED
    else:
      return messages.REGISTER_FINISH

  def __remove(self, message):
    # remove all transactions where the sender is user
    mongotools.transactions_db.delete_many({
        'sender': self.address
    }).deleted_count

    # unregister user
    self.registered = False
    return messages.START_MESSAGE_WITH_GROUP

  def __change(self, message):
    if len(get_unconfirmed_transactions_from_address(self.address)) != 0:
      return messages.CHANGED_FAILED_BECAUSE_TRANSACTIONS

    self.registered = False
    m = self.__register(message, changed=True)
    self.registered = True

    return m

  def __info(self, message):
    return messages.ADDRESS_IS % self.address

  def __getbalance(self, message):
    return messages.BALANCE_IS % blockchaintools.get_balance(self.address)

  def __list(self, message):
    transactions = tools.get_full_list_of_transactions(message.from_user.id)
    if len(transactions) == 0:
      return messages.EMPTY_TRANSACTION_LIST
    return messages.LIST_OF_TRANSACTIONS % '\n'.join(transactions)

  def __delete_transaction(self, message):
    try:
      assert len(message.text.split()) == 2
      _, id_to_delete = message.text.split()
    except (ValueError, AssertionError):
      return messages.WRONG_DELETE_TX_COMMAND

    return tools.try_delete_transaction(message.from_user.id, id_to_delete)

  def __reset(self, message):
    if message.from_user.username == BOT_ADMIN:
      mongotools.hard_reset()
      return messages.RESET_DONE
    return messages.RESET_FAILED

  def __deploy(self, message):
    if message.from_user.username == BOT_ADMIN:
      transaction_id = str(
          abs(hash(BOT_ADMIN + str(datetime.timestamp(datetime.now())))))
      link = tools.generate_deploy_url(transaction_id)
      mongotools.set_deployment_id(transaction_id)
      return messages.DEPLOY_LINK % link
    return messages.DEPLOY_FAILED

  def __set_contract(self, message):
    if message.from_user.username == BOT_ADMIN:
      try:
        assert len(message.text.split()) == 2
        _, contract_address = message.text.split()
        mongotools.set_contract_address(contract_address)
        return messages.SET_CONTRACT_DONE
      except (ValueError, AssertionError):
        return messages.WRONG_SET_CONTRACT_COMMAND
    return messages.SET_CONTRACT_FAILED

  def __get_contract(self, message):
    contract_address = mongotools.get_contract_address()
    if contract_address is not None:
      return messages.CONTRACT_ADDRESS % contract_address
    return messages.NO_PREMIUM_CONTRACT

  def __withdraw(self, message):
    if message.from_user.username == BOT_ADMIN:
      transaction_id = str(
          abs(hash(BOT_ADMIN + str(datetime.timestamp(datetime.now())))))
      link = tools.generate_withdraw_url(transaction_id,
                                         mongotools.get_contract_address())
      return messages.WITHDRAW_LINK % link
    return messages.WITHDRAW_FAILED

  def __premium(self, message):
    contract_address = mongotools.get_contract_address()
    logging.info(f'Contract: {contract_address}, address: {self.address}')

    if blockchaintools.is_premium(contract_address, self.address):
      return messages.ALREADY_PREMIUM

    if contract_address is None:
      return messages.NO_PREMIUM_CONTRACT
    amount = '5e17'
    transaction_id = str(
        abs(
            hash(self.address + contract_address + str(amount) +
                 str(datetime.timestamp(datetime.now())))))
    mongotools.get_mongo_db().transactions.insert_one({
        "_id":
        transaction_id,
        "sender":
        self.address,
        "receiver":
        contract_address,
        "amount":
        str(blockchaintools.toWei(amount, 'ether'))
    })
    return messages.PREMIUM_LINK % tools.generate_url(transaction_id,
                                                      contract_address, amount)

  def __available_withdraw(self, message):
    contract_address = mongotools.get_contract_address()
    if contract_address is not None:
      return messages.PREMIUM_CONTRACT_BALANCE % blockchaintools.get_balance(
          contract_address)
    return messages.NO_PREMIUM_CONTRACT

  def __send(self, message):
    if not self.registered:
      return messages.NOT_REGISTERED

    contract_address = mongotools.get_contract_address()

    if not blockchaintools.is_premium(contract_address, self.address):
      return messages.NOT_PREMIUM_ACCOUNT

    return tools.send_transaction(message, True)

  def __notification(self, message):
    if self.notification:
      self.notification = not self.notification
      return messages.NOTIFICATIONS_DISABLED
    else:
      self.notification = not self.notification
      return messages.NOTIFICATIONS_ENABLED

  def __listchats(self, message):
    if message.from_user.username == BOT_ADMIN:
      chats = return_db_stats_info()['chats']
      message_to_send = "None" if len(
          chats) == 0 else messages.BOT_REGISTERED_IN + '\n'
      for chat in chats:
        message_to_send += f"{chat['chat_id']}\n"
      return message_to_send
    else:
      return messages.RESET_FAILED

  def __listusers(self, message):
    if message.from_user.username == BOT_ADMIN:
      users = return_db_stats_info()['registered_users']
      message_to_send = message_to_send = "None" if len(
          users) == 0 else messages.USERS_IN_BOT + '\n'
      for user in users:
        message_to_send += f"{user['alias']}\n"
      return message_to_send
    else:
      return messages.RESET_FAILED

  def update(self, chat_id):
    mongotools.update_user(chat_id,
                           alias=self.alias,
                           address=self.address,
                           registered=self.registered,
                           notification=self.notification)

  def __str__(self):
    return f"{self.alias}: | REG: {self.registered} | IN_SOME_GROUP: {self.in_some_group}"
