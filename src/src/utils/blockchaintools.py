from web3 import Web3, HTTPProvider, exceptions
from utils import config

web3 = Web3(HTTPProvider(config.WEB3_PROVIDER))

CHAIN_ID = web3.eth.chainId


def checksum(address):
  return Web3.toChecksumAddress(address)


def check_pub_key(address):
  if not address.lower().startswith('0x'):
    raise ValueError(f'address should start with 0x: {address}')
  return checksum(address)


def is_premium(contract_address, address):
  abi = [{
      "constant": True,
      "inputs": [{
          "internalType": "address",
          "name": "",
          "type": "address"
      }],
      "name": "isPremium",
      "outputs": [{
          "internalType": "bool",
          "name": "",
          "type": "bool"
      }],
      "payable": False,
      "stateMutability": "view",
      "type": "function"
  }]
  contract = web3.eth.contract(address=contract_address, abi=abi)
  return contract.functions.isPremium(address).call()


def get_balance(address):
  return fromWei(web3.eth.getBalance(Web3.toChecksumAddress(address)), 'ether')


def toWei(amount: float, unit: str):
  return web3.toWei(amount, unit)


def fromWei(wei: int, unit):
  wei = str(wei)

  if len(wei) <= 18:
    wei = ('0' * (19 - len(wei))) + wei

  wei = list(str(wei))

  wei.insert(len(wei) - 18, '.')

  while wei[-1] == '0':
    wei.pop(-1)

  if wei[-1] == '.':
    wei.pop(-1)

  return ''.join(wei)
