# TODO: Remove russian comments

# START MESSAGE WHEN USER IS IN SOME CHAT
START_MESSAGE_WITH_GROUP = "Payment Bot welcomes you! To register in the bot, send ‘/register’ as a private message to the bot. Send ‘/help’ to get info for other actions."

# START MESSAGE WHEN USER IS NOT IN SOME CHAT
START_MESSAGE_WITHOUT_GROUP = "Payment Bot welcomes you! You are not recognized as a user participating in the group where deanonymization is required."

# Help message
HELP_MESSAGE = """/register <account> - to register yourself 
/remove - to un-register yourself
/change <account> - to change previosuly linked account
/info - to get information about your account
/list - to get the list of your transfers
/deltx <transfer id> - to delete a transfer request from the list
/getbalance - to get the balance
/notification - to turn on/off notifications about unused transfers"""

# Someone added bot to a chat
REGISTER_IN_BOT = "To have an ability to send money to participants please register in the bot %s"

# /help in chat
HELP_MESSAGE_IN_CHAT = '* To have an ability to send money to participants register in the bot %s\n'\
                       '* To transfer money to a participant send "send XX to @participantalias", where XX - value in ether\n'\
                       '* To get this message send "/help"'

# Ограничение регистрации пользователя, если он не состоит ни в одной из бесед, где зарегистрирован бот-активатор
NOT_IN_CHAT = "You are not recognized as a user participating in the group where bot is required."

# ALREADY REGISTERED
ALREADY_REGISTERED = "You have already registered."

# Пользователь попытался зарегаться но он уже зареган
REGISTER_FINISH = "Now you can send money to other people."

# Пользователь хочет удалиться но он не зареган
NOT_REGISTERED = "Not registered."

# Пользователь удалился из успешно
SUCCESSFUL_REMOVE = "Removed."

# Succesed change
CHANGED = "Now you will get payments to another account."

CHANGED_FAILED_BECAUSE_TRANSACTIONS = 'Sorry, you have unconfirmed transactions'

# Пользователь отправил некорректный адрес
ADDRESS_IS_INCORRECT = "Sorry, address is incorrect. Check it again."

# Не удалось понять что прислал пользователь
SEE_HELP = "Send '/help' to get info for other actions."

# Сообщение для баланса
BALANCE_IS = "Balance is %s"

# Пользователь попросил перевести эфир, но такого юзера нет
THERE_IS_NO_SUCH_USER = "User %s has not registered in the payment bot"

# Пользователь попросил перевести эфир, но
USER_NOT_IN_THIS_CHAT = "User %s is not in the chat"

# Message to receiver
#                                          amount alias                              link
URL_TO_TRANSACTION_IS = "To confirm transfer %s to %s open the link in the browser:\n%s"

# Сообщение юзеру что он получил транзакцию
#                               alias         amount
YOU_RECEIVED_TRANSACTION = "User %s sent to you %s"

# Transaction was failed
TRANSACTION_WAS_FAILED = "Transaction to %s was failed"

# User received transaction
#                           alias      amount
YOU_SENT_TRANSACTION = "User %s received %s from you"

#

# Запрещено отправлять транзакцию себе
FORBIDDEN_TO_SEND_TO_YOURSELF = "Specify the alias of another person from the chat"

# US-111
MORE_THAN_THREE_TRANSACTION = "You cannot requests more than 3 transfers without confirming previous requests."

# Information about accout
ADDRESS_IS = "Your account: %s"

# Send to user list of transactions
#                                       tras list
LIST_OF_TRANSACTIONS = "Your outstanding transfers:\n%s"

# Format of one transaction:
#                   id link
TRANSACTION_REPR = "%s: %s"

# User sent wrong /delete_tx command
WRONG_DELETE_TX_COMMAND = "Specify your tranfser id after /deltx command"

# Successful transaction deletion
#                                  id
TRANSACTION_DELETED = "Transaction %s was deleted"

# Successful hard reset
RESET_DONE = "Database dropped"

# Reset failed
RESET_FAILED = "You are not allowed to perform such request."

DEPLOY_LINK = "To confirm deployment: open the link in the browser:\n%s"

# Deployment not allowed or failed
DEPLOY_FAILED = "You are not allowed to deploy contract"

WRONG_SET_CONTRACT_COMMAND = "Invalid set contract command"

SET_CONTRACT_DONE = "Setting of contract completed"

SET_CONTRACT_FAILED = "Setting of contract failed"

WITHDRAW_LINK = "To confirm withdraw: open the link in the browser:\n%s"

WITHDRAW_FAILED = "Withdraw failed"

CONTRACT_DEPLOYED = "Contract deployed"

CONTRACT_ADDRESS = "Contract is deployed in %s"

ALREADY_PREMIUM = "Your account is already premium"

NO_PREMIUM_CONTRACT = "No premium contract defined"

PREMIUM_CONTRACT_BALANCE = "Premium contract balance is: %s"

PREMIUM_LINK = "To activate premium: open the link is the browser:\n%s"

PREMIUM_ACTIVATED = "Premium activated on your account"

NOT_PREMIUM_ACCOUNT = "Not a premium account"
# Reminder of transaction
REMINDER = "Do not forget about transaction %s"

NOTIFICATIONS_DISABLED = "Notifications are disabled"

NOTIFICATIONS_ENABLED = "Notifications are enabled"

EMPTY_TRANSACTION_LIST = "No outstanding transfers"

BOT_REGISTERED_IN = "Bot registered in groups:"

USERS_IN_BOT = "Users registered in the bot:"
