#!/usr/local/bin/python3

import pymongo


def return_db_stats_info():
  db = pymongo.MongoClient('mongodb://root:password@mongo:27017/').nti2019
  registered_users = list(db.users.find({'registered': True}))
  chats = list(db.chats.find({}))
  return {"registered_users": registered_users, "chats": chats}
