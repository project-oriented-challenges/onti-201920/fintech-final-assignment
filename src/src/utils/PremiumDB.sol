pragma solidity ^0.6.4;

contract PremiumDB {
    event PremiumPurchased(address);

    mapping(address => bool) public isPremium;

    receive() external payable {
        require(msg.value == 0.5 ether);
        
        isPremium[msg.sender] = true;
    }
}
