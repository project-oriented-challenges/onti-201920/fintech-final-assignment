import sys
from os.path import dirname
import typing
import math

sys.path.append(dirname(__file__))

synonyms = [['send', 'sends', 'transfer', 'give'],
            ['coin', 'coins', 'cryptocurrency', 'token', 'tokens']]

useful_words = ["wallet", "address", "from"]


def index(array, elem):
  return array.index(elem) if elem in array else -1


def get_indexes_of_main_part(sentence: typing.List[str]):
  send_word_index = -1
  alias_index = -1
  currency_index = -1
  number_index = -1

  # get index of send
  for synonym in synonyms[0]:
    ind = index(sentence, synonym)
    if ind != -1:
      send_word_index = ind
      break

  # get index of currency specifier
  for synonym in synonyms[1]:
    ind = index(sentence, synonym)
    if ind != -1:
      currency_index = ind
      break

  # get index of the first digit and alias
  for ind, item in enumerate(sentence):
    try:
      float(item)
      assert number_index == -1
      number_index = ind
    except:
      pass

    if item.startswith("@") and alias_index == -1:
      alias_index = ind

  return [send_word_index, alias_index, number_index, currency_index]


def remove_stop_words(sentence: typing.List[str]):
  clear_sentence = sentence
  with open('utils/nlp_task/stop_words.txt') as f:
    for word in f.readlines():
      word = word.strip()
      while word in clear_sentence:
        clear_sentence.remove(word)

  return clear_sentence


def wrong_structure_or_wrong_order(send_word_index, alias_index, number_index,
                                   currency_index):
  if currency_index != -1 and number_index > currency_index:
    return True
  if send_word_index == -1 and (number_index <= 1 and alias_index != -1):
    return False
  if send_word_index == -1 or alias_index == -1 or number_index == -1 or \
          send_word_index > min(alias_index, number_index):
    return True


def wrong_currency(sentence, alias_index, number_index, currency_index):
  # check that there is nothing between 10 and coins
  if currency_index != -1:
    return abs(currency_index - number_index) != 1

  # check that there is nothing after number if alias was before: send @Alexander 100 usd
  if number_index > alias_index and len(sentence[number_index + 1:]) > 0:
    return True

  # Send 10 bananas from my wallet to @Alexander
  words_between = sentence[number_index + 1:alias_index]
  to_index = index(words_between, 'to')

  if to_index != -1:
    ind = number_index + to_index + 1
  else:
    ind = alias_index

  return len(set(sentence[number_index + 1:ind]) - set(useful_words)) > 0


def check_distances(sentence, alias_index, number_index, currency_index):
  useful_words_count = 0
  for ind, elem in enumerate(sentence):
    if elem in useful_words:
      useful_words_count += 1

  distance = (abs(alias_index - number_index) - useful_words_count)**2
  distance += (number_index - currency_index)**2

  return distance


def has_negation_before_send_message(sentence):
  for word in ['cannot', 'dont', "don't", 'wont', 'not']:
    ind = index(sentence, word)
    if ind != -1:
      return True


def contains_not_present_simple(sentence, send_index):
  not_present_simple = [
      'yesterday', 'will', 'transferred', 'sent', 'sending', 'gave'
  ]
  for ind, word in enumerate(not_present_simple):
    if word in sentence:
      return True
    if ind > send_index != -1:
      break


def is_send_sentence(sentence: str) -> bool:
  if sentence is None:
    return False
  sentence = sentence.lower().replace(',', '')
  sentence = sentence[:-1] if sentence[-1] == "." else sentence
  sentence = sentence.split()
  sentence = remove_stop_words(sentence)
  [send_word_index, alias_index, number_index,
   currency_index] = get_indexes_of_main_part(sentence)

  if wrong_structure_or_wrong_order(send_word_index, alias_index, number_index,
                                    currency_index):
    return False
  if contains_not_present_simple(sentence, send_word_index):
    return False
  if has_negation_before_send_message(sentence):
    return False
  if wrong_currency(sentence, alias_index, number_index, currency_index):
    return False

  return True


def grade(fun, correct_sentences, fail_sentences):
  correct = 0
  for sentence in correct_sentences:
    if fun(sentence):
      correct += 1
    else:
      print(sentence, fun(sentence))

  for sentence in fail_sentences:
    if not fun(sentence):
      correct += 1
    else:
      print(sentence, fun(sentence))

  accuracy = correct / (len(correct_sentences) +
                        len(fail_sentences)) * 100 - 50
  print("accuracy differs from random in", accuracy)

  return 30 * (1 / (1 + math.e**(-accuracy / 3 + 10)))


if __name__ == '__main__':
  true_sentences = [
      'Send 1 to @Alexander',
      'Transfer 1 to @Alexander',
      'Send to @Alexander wallet 1',
      '10 tokens to @Alexander',
      'Transfer the payment of 10 tokens to @Alexander address',
      'I want to send 15 to @Alexander wallet',
      'Hello everyone, I want to send 1.1 to @frankmarsFinTech',
      'Send 1 to @Alexander address',
      'Hello, I want to send @Alexander money in currency 1 token',
      'Hello, I want to send @Alexander money in currency 2 token',
      'Hello, I want to send @Alexander money in currency 3 token',
      'Hello, I want to send @Alexander money in currency 4 token',
      'Hello, I want to send @Alexander money in currency 5 token',
      'Now I want to send 10 to address wallet @Alexander',
      'Please send 0.05 to this alias @Alexander wallet address now',
      'Please transfer payment of 2 coins from my wallet to @Alexander',
      'Dear bot, please transfer payment of 2 coins from my wallet to @Alexander',
      'I want to give 200 tokens to the best participant @null',
      'Bot, please, transfer 1 coin to @alexander',
      'By the way, transfer 1 coin to @alexander',
      'Hey, payment bot, could you give 1 to my friend @alexander',
      'Today I got my salary! Bot, transfer payment of 1 from my beautiful wallet to @alexander',
      'It would be very great if bot sends @alexander 1 coin',
      'Hmm, It would be very great if bot sends @alexander 1 coin',
      'I always wanted to send @alexander my 10 coins debt, so bot, could you please do it',
  ]

  false_sentences = [
      'Sent to this alias @Alexander 1 token, waiting for response',
      'Send to @Alexander 100 usd', 'Do not send 10 to @Alexander'
      'Do not send 100 to @Alexander'
      'Send coins 10 to @Alexander',
      'I would like to give my 1 banana to @alexaneer',
      '@Alexander, please send me 16', '@Alexander, please send me 17',
      'Please do not send @Alexander 10', "Don't send any money to @Alexander",
      'Send 10 bananas from my wallet to @Alexander',
      'I dont want to send @Alexander 0.6 tokens.',
      'Gave @Alexander 10 coins to test our nlp system, hope it works',
      'Yesterday, we sent with @Alexander 1 payment of 200 in cryptocurrency.',
      'Seriously, I do not remember that I have to send 1 coin to @alexander',
      '@All, could you send 2 to me, my alias is @alexander',
      'Today I saw that @Matvey sent 1 to @Alexander',
      '@Alexander, could you please send 1 to @Matvey',
      'I forgot to pay 2 tokens for electricity, @alexander',
      "I don't know how to send 1 coin to @alexander",
      'Yesterday some of you forgot to send 1 coin to @alexander',
      'I would like to spend my 3 coins for @alexander',
      'One day I will send 1 coin to @alexander',
      'sending 1 coin from my wallet to @alexander is really bad',
      'Sorry, I cannot transfer 1 token to @alexander',
      "why don't you want to send 1 coin to @alexander?",
      "I would not like to transfer 3 coins to your wallet @alexander"
  ]

  print(grade(is_send_sentence, true_sentences, false_sentences))
  print("True: ", len(true_sentences))
  print("False: ", len(false_sentences))
