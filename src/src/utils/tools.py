import re
from typing import List
from telebot import TeleBot
from datetime import datetime
import logging

from ReminderModule import queue as notificationQueue
from ServerModule.Server import insert_transaction_details, 
                                get_unconfirmed_transactions_from_address
from BotModule import Log
from utils import mongotools
from utils.User import User
from utils import telegramtools
from utils.blockchaintools import toWei, fromWei, CHAIN_ID
from utils import messages
from utils.config import regexp_for_transaction, WEB_HOST, WEB_PORT, 
                         AMOUNT_OF_SYMBOLS_TO_COMPARE_IDS, SECONDS_TO_WAIT


def process_update(message) -> str:
  """
    Return a message to send to the user 
    in response to a command or text
    """

  # Message was sent to a PM, so create new user
  if not mongotools.user_in_database(message.from_user.username):
    db_user = mongotools.create_new_user(
        message.from_user.id, normalize_alias(message.from_user.username))
  else:
    db_user = mongotools.get_user(message.from_user.id)

  # Create USER instance
  user = User(alias=db_user['alias'],
              registered=db_user['registered'],
              address=db_user['address'],
              in_some_group=user_in_some_chat(message.from_user.id),
              notification=db_user['notification'])

  # Do transitions
  text_to_send = user.action(message)

  # Update database
  user.update(message.from_user.id)

  return text_to_send


def send_transaction(message, force=False):
  # TODO: description of the method

  user_from = mongotools.get_user(message.from_user.username)

  # If user_from NOT in database
  if not user_from['registered']:
    return messages.REGISTER_IN_BOT % telegramtools.get_alias()

  amount, alias = parse_text(message.text)
  user_to = mongotools.get_user(alias)

  # If user_to NOT in database
  if not user_to['registered']:
    return messages.THERE_IS_NO_SUCH_USER % alias

  # If user_to is NOT in this chat
  if not telegramtools.user_in_chat(message.chat.id,
                                    user_to['chat_id']) and not force:
    return messages.USER_NOT_IN_THIS_CHAT % alias

  # If user_to in this chat and
  # all users are registered in the bot

  address_from, address_to = user_from['address'], user_to['address']

  # If user decided to send transaction to your self
  if address_from == address_to:
    return messages.FORBIDDEN_TO_SEND_TO_YOURSELF

  # TODO: amount of transaction less than 3
  if len(get_unconfirmed_transactions_from_address(address_from)) >= 3:
    telegramtools.send_message(message.from_user.id,
                               messages.MORE_THAN_THREE_TRANSACTION)
    return

  # Unique and non-predictable ID
  transaction_id = str(
      abs(
          hash(address_from + address_to + str(amount) +
               str(datetime.timestamp(datetime.now())))))

  # Send transacation to database
  transaction = insert_transaction_details(transaction_id=transaction_id,
                                           sender=address_from,
                                           receiver=address_to,
                                           amount=str(toWei(amount, 'ether')),
                                           start_time=datetime.timestamp(
                                               datetime.now()))

  if user_from['notification']:
    notificationQueue.put(transaction)

  # Generate url
  url = generate_url(transaction_id, address_to, toWei(amount, 'ether'))

  # Send to PM url
  telegramtools.send_message(
      message.from_user.id,
      messages.URL_TO_TRANSACTION_IS % (amount, alias, url))


def restore_link(transaction_id):
  transaction = mongotools.transactions_db.find_one({"_id": transaction_id})

  if transaction is None:
    return "TODO: импортировать из messages сообщение об ошибке"

  address, amount = transaction['receiver'], transaction['amount']

  return generate_url(transaction_id, address, amount)


def generate_url(transaction_id, address, amount):
  URL = f'http://{WEB_HOST}:{WEB_PORT}/?id={transaction_id}&link="ethereum:{address}@{CHAIN_ID}?value={amount}"'
  return URL


def generate_deploy_url(transaction_id):
  bytecode = '0x608060405234801561001057600080fd5b50600180546001600160a01b0319163317905561017a806100326000396000f3fe6080604052600436106100295760003560e01c80633ccfd60b1461008c578063742fe0d4146100a3575b346706f05b59d3b200001461003d57600080fd5b3360008181526020818152604091829020805460ff19166001179055815192835290517f8635c858710dee3b9bb5e5bc7fcc805c455795162aa5b74dd4a508961a3603239281900390910190a1005b34801561009857600080fd5b506100a16100ea565b005b3480156100af57600080fd5b506100d6600480360360208110156100c657600080fd5b50356001600160a01b0316610130565b604080519115158252519081900360200190f35b6001546001600160a01b0316331461010157600080fd5b60405133904780156108fc02916000818181858888f1935050505015801561012d573d6000803e3d6000fd5b50565b60006020819052908152604090205460ff168156fea265627a7a72315820eda891b1ac09382ff772e3fe8295f8142b5ae64f7683299c533acc6a9aa6edc164736f6c63430005110032'
  URL = f'http://{WEB_HOST}:{WEB_PORT}/?id={transaction_id}&link="ethereum:0x@{CHAIN_ID}?data={bytecode}"'
  return URL


def generate_withdraw_url(transaction_id, contract):
  return f'http://{WEB_HOST}:{WEB_PORT}/?id={transaction_id}&link="ethereum:{contract}@{CHAIN_ID}/withdraw"'


def get_function_with_bot_instance(bot_instance: TeleBot):
  '''
    :param bot_instance: bot that supports sending messages
    :return:
    '''
  def accept_result_from_server(res):
    '''
        :param res: dictionary with transaction info if succeed
        '''

    sender_address = res['sender_address']
    receiver_address = res['receiver_address']
    amount = res['amount']

    if res['contract_address'] != '' and res['contract_address'] != None:
      logging.info('Deployed contract')
      return

    if res['data'] == '0x3ccfd60b':
      return

    if receiver_address == mongotools.get_contract_address():
      user_sender = mongotools.get_user(sender_address)
      sender_chat_id = user_sender['chat_id']
      bot_instance.send_message(chat_id=sender_chat_id,
                                text=messages.PREMIUM_ACTIVATED)
      return

    user_sender = mongotools.get_user(sender_address)
    user_receiver = mongotools.get_user(receiver_address)

    if not user_sender['registered']:
      logging.error(
          f"The transaction was confirmed, but there is no user_sender with address {sender_address}"
      )
      return

    if not user_receiver['registered']:
      logging.error(
          f"The transaction was confirmed, but there is no user_receiver with address {receiver_address}"
      )
      return

    receiver_chat_id, alias_sender, amount = user_receiver[
        'chat_id'], normalize_alias(user_sender['alias']), fromWei(
            amount, 'ether')
    sender_chat_id, alias_receiver = user_sender['chat_id'], normalize_alias(
        user_receiver['alias'])

    # Message to receiver
    bot_instance.send_message(chat_id=receiver_chat_id,
                              text=messages.YOU_RECEIVED_TRANSACTION %
                              (alias_sender, amount))

    # Message to sender
    bot_instance.send_message(chat_id=sender_chat_id,
                              text=messages.YOU_SENT_TRANSACTION %
                              (alias_receiver, amount))

  return accept_result_from_server


def get_full_list_of_transactions(chat_id):
  user_address = mongotools.get_user(chat_id)['address']

  # db = mongotools.get_mongo_db().transactions
  transactions = []

  for transaction in get_unconfirmed_transactions_from_address(user_address):
    _id = transaction['_id']

    link = restore_link(_id)

    transactions.append(messages.TRANSACTION_REPR %
                        (_id[:AMOUNT_OF_SYMBOLS_TO_COMPARE_IDS], link))

  return transactions


def try_delete_transaction(chat_id, transaction_id):
  """
    Delete transaction with `transaction_id`, 
    only if `sender` == `address from user`    
    """
  user_address = mongotools.get_user(chat_id)['address']

  transaction = mongotools.get_transaction(transaction_id, user_address)

  if transaction is None:
    return messages.WRONG_DELETE_TX_COMMAND

  mongotools.transactions_db.delete_one(transaction)
  return messages.TRANSACTION_DELETED % transaction_id


def parse_text(text):
  """
    Parse text and return amount and alias
    """
  alias = None
  amount = None

  for word in text.split():
    if word.startswith("@") and not alias:
      alias = word
    try:
      assert amount is None
      amount = float(word)
      if amount % 1 == 0:
        amount = int(amount)
    except:
      pass

  return amount, normalize_alias(alias)


def user_in_some_chat(user_id) -> bool:
  """
    Check all chats and return true if user is in some chat
    """

  for chat in mongotools.range_chats():
    if telegramtools.user_in_chat(chat['chat_id'], user_id):
      return True
  return False


def normalize_alias(alias, remove=True) -> str:
  """
    Returns alias with '@'
    """
  if remove:
    return '@' + str(alias).replace('@', '')
  return str(alias).replace('@', '')


def get_register_message(in_group=False):
  if in_group:
    return messages.HELP_MESSAGE_IN_CHAT % telegramtools.get_alias()

  return messages.REGISTER_IN_BOT % telegramtools.get_alias()


def check_for_notifications(transaction):
  if mongotools.get_mongo_db().confirmed_transactions.find_one(
      {"_id": transaction["_id"]}):
    return True

  if not mongotools.get_mongo_db().transactions.find_one(
      {"_id": transaction["_id"]}):
    return True

  now = datetime.timestamp(datetime.now())
  Log.info(
      f"Checking for transaction {transaction['_id']}. {now - transaction['start_time']} second(s) left."
  )
  if now - transaction['start_time'] > SECONDS_TO_WAIT:
    user = mongotools.get_user(transaction['sender'])
    if user['registered'] and user['notification']:
      Log.info(f"Send to {user['alias']} notification")
      telegramtools.notify_about_transaction(user['chat_id'],
                                             transaction['_id'])
    return True

  return False
