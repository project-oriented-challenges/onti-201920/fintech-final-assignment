import pymongo
import logging
from utils import tools
from typing import Dict, List
from utils.config import AMOUNT_OF_SYMBOLS_TO_COMPARE_IDS


def get_mongo_client():
  return pymongo.MongoClient('mongodb://root:password@mongo:27017/')


def get_mongo_db():
  #return pymongo.MongoClient().nti2019
  return get_mongo_client().nti2019


default_user = {
    'chat_id': None,
    'alias': None,
    'address': None,
    'registered': False,
    'notification': False
}

default_chat = {'chat_id': None}


def create_new_user(chat_id, alias="", address=None):
  # If user already exists
  user = users_db.find_one({'chat_id': chat_id})
  if not user is None:
    return user

  user = default_user.copy()
  user['chat_id'] = chat_id
  user['alias'] = tools.normalize_alias(alias)
  user['address'] = address

  users_db.insert_one(user)
  return user


def create_new_chat(chat_id):
  # If chat already exists
  chat = chats_db.find_one({'chat_id': chat_id})
  if not chat is None:
    return chat

  chat = default_chat.copy()
  chat['chat_id'] = chat_id
  chats_db.insert_one(chat)
  return chat


def get_user(information) -> Dict:
  """
    Returns all information about user
    """

  for user_iter in users_db.find({'alias':
                                  tools.normalize_alias(information)}):
    return user_iter

  for user_iter in users_db.find({'chat_id': information}):
    return user_iter

  for user_iter in users_db.find({'address': information}):
    return user_iter

  return default_user.copy()


def get_transaction(transaction_id, user_address):
  """
    :param transaction_id: first 8 symbols of transaction id
    :param sender: wallet address of user that is trying to get information
    """

  for transaction in transactions_db.find({}):
    _id, sender = transaction['_id'], transaction['sender']
    if _id[:
           AMOUNT_OF_SYMBOLS_TO_COMPARE_IDS] == transaction_id and sender == user_address:
      return transaction


def user_in_database(alias) -> bool:
  return not (get_user(alias)['chat_id'] is None)


def range_chats():
  for chat_iter in chats_db.find({}):
    yield chat_iter


def update_user(chat_id,
                alias=None,
                address=None,
                registered=None,
                notification=None):
  new_user_options = {}
  for option_name, option in zip(
      ['alias', 'address', 'registered', 'notification'],
      [alias, address, registered, notification]):
    if not option is None:
      new_user_options[option_name] = option

  users_db.update_one({'chat_id': chat_id}, {'$set': new_user_options})


def delete_chat(chat_id):
  """
    delete group(chat) from the database
    """

  return chats_db.find_one_and_delete({'chat_id': chat_id})


def hard_reset():
  """
    Hard reset to virgin state
    """

  users_db.drop()
  chats_db.drop()
  transactions_db.drop()
  contracts_db.drop()


def set_deployment_id(id):
  contracts_db.drop()
  contracts_db.insert_one({'deployment_id': id})


def set_contract_address(address, id=None):
  if id is None:
    contracts_db.drop()
    contracts_db.insert_one({'address': address})
  else:
    contracts_db.update_one(
        {'deployment_id': id},
        {'$set': {
            'address': address,
            'deployment_id': id
        }})


def get_contract_address():
  contract = contracts_db.find_one({})
  logging.info(f'contract: {contract}')
  return None if contract is None else contract.get('address', None)


users_db = get_mongo_db().users
chats_db = get_mongo_db().chats
transactions_db = get_mongo_db().transactions
contracts_db = get_mongo_db().contracts

print('[INFO] >> Database has been attached\n')
