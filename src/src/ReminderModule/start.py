from ReminderModule import queue
import time
from utils.tools import check_for_notifications


def start():
  while True:
    if not queue.empty():
      transaction = queue.get()
      if not check_for_notifications(transaction):
        queue.put(transaction)

    time.sleep(1)
