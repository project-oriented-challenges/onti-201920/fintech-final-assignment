function parseURL() {
    const search = window.location.search
    const id = search.match(/id=[a-zA-Z0-9]+/)[0].slice(3)
    const link = search.match(/link=%22.+%22/)[0]
    return [id, parseTransactionURL(link.slice(8, link.length - 3))]
}

function parseTransactionURL(uri) {
    if (!uri || typeof uri !== 'string') {
        throw new Error('uri must be a string')
    }

    if (uri.substring(0, 9) !== 'ethereum:') {
        throw new Error('Not an Ethereum URI')
    }

    let prefix
    let address_regex = '(0x[\\w]{40}|0x)'


    if (uri.substring(9, 11).toLowerCase() === '0x') {
        prefix = null
    } else {
        let cutOff = uri.indexOf('-', 9)

        if (cutOff === -1) {
            throw new Error('Missing prefix')
        }
        prefix = uri.substring(9, cutOff)
        const rest = uri.substring(cutOff + 1)

        // Adapting the regex if ENS name detected
        if (rest.substring(0, 2).toLowerCase() !== '0x') {
            address_regex = '([a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,})'
        }
    }

    const full_regex = '^ethereum:(' + prefix + '-)?' + address_regex + '\\@?([\\w]*)*\\/?([\\w]*)*'

    const exp = new RegExp(full_regex)
    const data = uri.match(exp)
    if (!data) {
        throw new Error('Couldnot not parse the url')
    }

    let parameters = uri.split('?')
    parameters = parameters.length > 1 ? parameters[1] : ''
    const params = Qs.parse(parameters)

    const obj = {
        scheme: 'ethereum',
        target_address: data[2]
    }

    if (prefix) {
        obj.prefix = prefix
    }

    if (data[3]) {
        obj.chain_id = data[3]
    }

    if (data[4]) {
        obj.function_name = data[4]
    }

    if (Object.keys(params).length) {
        obj.parameters = params
        const amountKey = obj.function_name === 'transfer' ? 'uint256' : 'value'

        if (obj.parameters[amountKey]) {
            obj.parameters[amountKey] = new BigNumber(obj.parameters[amountKey], 10).toString()
            if (!isFinite(obj.parameters[amountKey])) throw new Error('Invalid amount')
            if (obj.parameters[amountKey] < 0) throw new Error('Invalid amount')
        }
    }

    return obj
}

async function sendTx(tx) {
    return await new Promise((res, rej) => ethereum.sendAsync({
        method: 'eth_sendTransaction',
        params: [tx],
        from: ethereum.selectedAddress,
    }, (err, result) => err ? rej(err) : res(result)))
}

const MESSAGES = {
    TX_SEND: "Transfer confirmed",
    NO_METAMASK: 'Please install MetaMask',
    WRONG_CHAIN: 'Transfer targeted to different chain',
    BAD_URL: 'Incorrect url format',
    BAD_SENDER: 'Incorrect Metamask address',
    TX_HANDLED_COMPLETED: 'Confirmation link already used',
    TX_REJECTED: 'Transfer rejected',
    TX_PENDING: 'Wait for transfer accomplishment',
    TX_FAILED: 'Transfer failed',
    TX_HANDLED_FAILED: 'Confirmation link already used',
}

async function main() {
    if (typeof web3 !== "undefined") {
        window.web3 = new Web3(web3.currentProvider)
    } else {
        alert(MESSAGES.NO_METAMASK)
        return
    }

    try {
        await ethereum.enable()
        await new Promise(res => setTimeout(res, 500))
    } catch {
        alert(MESSAGES.NO_METAMASK)
        return
    }

    let id, tx
    try {
        [id, tx] = parseURL()
    } catch {
        alert(MESSAGES.BAD_URL)
        return
    }

    if (parseInt(tx.chain_id, 10) !== parseInt(ethereum.chainId.slice(2), 16)) {
        alert(MESSAGES.WRONG_CHAIN)
        return
    }

    // deployment
    if (tx.parameters && tx.parameters.data) {
        const transactionParameters = {
            gasPrice: '2540be400',
            gas: '95208',
            to: '',
            from: ethereum.selectedAddress,
            value: '0',
            data: tx.parameters.data,
            chainId: tx.chain_id
        }

        try {
            const response = await sendTx(transactionParameters)

            await fetch('/confirm', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id,
                    txHash: response.result
                })
            })
        } catch(e) {
            console.log(e)
        }
        return
    }

    // withdraw call
    if (tx.function_name) {
        const transactionParameters = {
            gasPrice: '2540be400',
            gas: '95208',
            to: tx.target_address,
            from: ethereum.selectedAddress,
            value: '0',
            data: '0x3ccfd60b', // withdraw()
            chainId: tx.chain_id
        }

        try {
            const response = await sendTx(transactionParameters)

            await fetch('/confirm', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id,
                    txHash: response.result
                })
            })
        } catch(e) {
            console.log(e)
        }
        return
    }

    const status = await (await fetch('/checkId', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            id,
            sender: ethereum.selectedAddress
        })
    })).text()

    if (status === 'completed') {
        alert(MESSAGES.TX_HANDLED_COMPLETED)
        return
    } else if (status === 'pending') {
        alert(MESSAGES.TX_PENDING)
        return
    } else if (status === 'failed') {
        alert(MESSAGES.TX_HANDLED_FAILED)
        return
    } else if (status === 'bad_sender') {
        alert(MESSAGES.BAD_SENDER)
        return
    }

    const transactionParameters = {
        gasPrice: '2540be400',
        gas: '15208',
        to: tx.target_address,
        from: ethereum.selectedAddress,
        value: BigNumber(tx.parameters.value, 10).toString(16),
        data: '',
        chainId: tx.chain_id
    }

    try {
        const response = await sendTx(transactionParameters)

        document.getElementById('id').innerHTML = id
        document.getElementById('hash').innerHTML = response.result
        document.getElementById('link').innerHTML = `<a href='https://kovan.etherscan.io/tx/${response.result}'>link</a>`


        await fetch('/confirm', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id,
                txHash: response.result
            })
        })

        alert(MESSAGES.TX_SEND)

        while (true) {
            await new Promise(res => setTimeout(res, 300))
            const receipt = await web3.eth.getTransactionReceipt(response.result)
            console.log(receipt)
            if (receipt !== null) {
                if (receipt.status === '0' || receipt.status === false) {
                    alert(MESSAGES.TX_FAILED)
                } else {
                    console.log('tx included into block')
                }
                return
            }
        }
    } catch (e) {
        document.getElementById('hash').innerHTML = 'Tx was rejected by user'
        alert(MESSAGES.TX_REJECTED)
    }
}
