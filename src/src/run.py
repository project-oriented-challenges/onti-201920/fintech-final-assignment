# TODO: run bot
import threading

from BotModule.bot import bot
from BotModule import Log
from ServerModule.Server import Server
from utils.tools import get_function_with_bot_instance
from utils.config import WEB_PORT
from ReminderModule.start import start as ReminderStart

if __name__ == "__main__":
  Log.info(
      f'==========================\nHello from @{bot.get_me().username}!\n==========================\n'
  )

  Log.info('[INFO] >> Removing webhook\n')
  bot.delete_webhook()

  Log.info('[INFO] >> Creating server')
  s = Server(WEB_PORT, get_function_with_bot_instance(bot))

  Log.info('[INFO] >> Starting thread with server')
  t = threading.Thread(target=s.run)
  t.start()

  Log.info('[INFO] >> Starting reminder thread')
  reminder_thread = threading.Thread(target=ReminderStart)
  reminder_thread.start()

  Log.info('[INFO] >> Starting polling ...\n')
  bot.polling()
