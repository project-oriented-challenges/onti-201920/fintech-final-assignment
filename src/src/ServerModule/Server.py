from http.server import BaseHTTPRequestHandler, HTTPServer
import logging
import os
import json
from utils import mongotools, blockchaintools
from queue import Queue
import datetime
from web3 import Web3

TRANSACTION_TIMEOUT = 3  # minutes
HTTP_REQUEST_TIMEOUT = 2  # seconds


class Server:
  def __init__(self,
               port: int,
               return_result: callable,
               transaction_pooling_period=2):
    self.queue = Queue()
    self.timeouts = {}
    self.port = port
    self.return_result = return_result
    self.transaction_pooling_period = transaction_pooling_period

  def run(self):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', self.port)
    httpd = HTTPServer(server_address, fun(self.queue))
    logging.info('Starting httpd...\n')
    httpd.timeout = HTTP_REQUEST_TIMEOUT  # seconds

    while True:
      httpd.handle_request()
      if not self.queue.empty():
        self.process_transaction()

  def process_transaction(self):
    item = self.queue.get(block=True)
    data = json.loads(item.decode())
    transaction_id, tx = data["id"], data["txHash"]
    if tx not in self.timeouts:
      logging.info(f"Put new transaction {tx} with timer")
      self.timeouts[tx] = datetime.datetime.now() + datetime.timedelta(
          minutes=TRANSACTION_TIMEOUT)
    data = wait_for_tx_and_extract_data(transaction_id, tx)

    if data is None:
      if self.timeouts[tx] > datetime.datetime.now():
        self.queue.put(item)
      else:
        del self.timeouts[tx]
        logging.error(
            f"No mined transaction in the chain during {TRANSACTION_TIMEOUT} minutes"
        )
    else:
      del self.timeouts[tx]
      if data['contract_address'] != None:
        logging.debug('Processing deployment tx')
        mongotools.set_contract_address(data['contract_address'],
                                        id=transaction_id)
        self.return_result(data)
        return
      if data['data'] != '0x':
        logging.debug('Processing withdraw tx')
        self.return_result(data)
        return
      db = mongotools.get_mongo_db().transactions
      removed = db.find_one({
          "_id": transaction_id,
          "sender": data["sender_address"],
          "receiver": data["receiver_address"],
          "amount": data["amount"]
      })
      if not removed:
        logging.error(json.dumps(data))
        logging.error("Unknown transaction")
      else:
        logging.error("return results")
        logging.error(json.dumps(data))
        self.return_result(data)


def wait_for_tx_and_extract_data(transaction_id, tx):
  w3 = Web3(Web3.HTTPProvider(os.environ["WEB3_PROVIDER"]))
  try:
    receipt = w3.eth.getTransaction(tx)
    if receipt['blockNumber'] is None:
      logging.info("Transaction was not mined yet")
      return
  except:
    logging.error("Transaction is not in the transaction pool")
    return
  receipt = w3.eth.getTransaction(tx)
  res = {
      "sender_address": receipt["from"],
      "receiver_address": receipt["to"],
      "amount": str(receipt["value"]),
      "data": receipt['input']
  }

  receipt = w3.eth.getTransactionReceipt(tx)
  status = receipt["status"]
  res['contract_address'] = receipt['contractAddress']

  # update status of a mined transaction
  confirmed_db = mongotools.get_mongo_db().confirmed_transactions
  confirmed_db.update_one({
      "_id": transaction_id,
      "txHash": tx
  }, {"$set": {
      "mined": True,
      "status": status
  }})
  if status == 1:
    return res


def fun(shared_queue):
  class RequestHandler(BaseHTTPRequestHandler):
    def do_POST(self):
      logging.info(self.path)
      content_length = int(self.headers['Content-Length'])
      post_data = self.rfile.read(content_length)
      logging.info(post_data)

      self.send_response(200)
      self.send_header('Content-type', 'text/plain')
      self.end_headers()

      confirmed_db = mongotools.get_mongo_db().confirmed_transactions
      transaction_db = mongotools.get_mongo_db().transactions
      data = json.loads(post_data.decode())
      if self.path == "/confirm":
        shared_queue.put(post_data)
        logging.info("new confirmed request")
        logging.info({
            "_id": data["id"],
            "txHash": data["txHash"],
            "mined": False,
            "status": 0
        })
        confirmed_db.insert_one({
            "_id": data["id"],
            "txHash": data["txHash"],
            "mined": False,
            "status": 0
        })
      elif self.path == "/checkId":
        confirmed_res = confirmed_db.find_one({"_id": data["id"]})
        sender = blockchaintools.checksum(data["sender"])
        transaction_res = transaction_db.find_one({
            "_id": data["id"],
            "sender": sender
        })
        logging.info(confirmed_res)
        logging.info(transaction_res)
        if not transaction_res:
          logging.error("bad_sender")
          response = "bad_sender"
        elif not confirmed_res:
          logging.error("ok")
          response = "ok"
        else:
          mined = confirmed_res["mined"]
          if mined:
            status = confirmed_res["status"]
            if status == 1:
              logging.error("completed")
              response = "completed"
            else:
              logging.error("failed")
              response = "failed"
          else:
            logging.error("pending")
            response = "pending"

        self.wfile.write(response.encode())

      # save info that user clicks button to inform him that this transaction is already sent

    def do_GET(self):
      if self.path.endswith(".html") or self.path.endswith(".js"):
        filename = self.path
      elif self.path == "/":
        filename = "welcome.html"
      else:
        filename = "index.html"

      f = open('frontend/public/' + filename, 'rb')
      self.send_response(200)
      self.send_header('Content-type', 'text/html')
      self.end_headers()
      self.wfile.write(f.read())
      f.close()

  return RequestHandler


def insert_transaction_details(transaction_id, sender, receiver, amount,
                               start_time):
  '''
    :param transaction_id: unique id generated for this transaction
    :param sender: wallet address of sender
    :param receiver: wallet address of receiver
    :param amount: amount in wei
    '''
  db = mongotools.get_mongo_db().transactions
  logging.info("new request")
  transaction = {
      "_id": transaction_id,
      "sender": sender,
      "receiver": receiver,
      "amount": amount,
      "start_time": start_time
  }
  logging.info(transaction)
  db.insert_one(transaction)
  return transaction


def get_unconfirmed_transactions_from_address(address):
  confirmed_db = mongotools.get_mongo_db().confirmed_transactions
  transaction_db = mongotools.get_mongo_db().transactions

  transaction_from_address = list(transaction_db.find({"sender": address}))
  unconfirmed_results = []
  for transaction in transaction_from_address:
    if not confirmed_db.find_one({"_id": transaction["_id"]}):
      unconfirmed_results.append(transaction)

  return unconfirmed_results


if __name__ == '__main__':
  # db = mongotools.get_mongo_db().transactions.delete_one({"_id": 1})
  # insert_transaction_details(transaction_id=1,
  #                            sender="0x7BD556B838a9E330a815a53EF39e04Ff2fDf2392",
  #                            receiver="0xA5a70346e986a1092cf852665eaD708b32efE0C1",
  #                            amount=2255856000000000)
  # w3 = Web3(Web3.HTTPProvider(os.environ["WEB3_PROVIDER_KOVAN"]))
  # receipt = w3.eth.getTransaction(tx)
  s = Server(8000, logging.info)
  s.run()
  '''
    To test
    curl --data '{"id":1, "txHash": "0xf0dccdd7aec936007fa70e43dd177e47224da862e53af723b0a6e76349c501c1"}' localhost:8080
    '''
